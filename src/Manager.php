<?php

use Entities\ModulePart;
use Entities\User;

class Manager
{

    /**
     * @param User $user
     * @param ModulePart $part
     * @return bool
     */
    public function can(User $user, ModulePart $part)
    {
        return (bool) (count(array_intersect($user->getPermissions(), $part->getAccessList())) > 0);
    }
}