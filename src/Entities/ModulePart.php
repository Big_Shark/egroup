<?php

namespace Entities;

class ModulePart
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var Module
     */
    private $module;

    /**
     * @var Permission[]
     */
    private $accessList;

    /**
     * @param Module $module
     * @param $name
     * @param $accessList
     */
    public function __construct(Module $module, $name, $accessList)
    {
        $this->name = $name;
        $this->module = $module;
        $this->accessList = $accessList;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Permission[]
     */
    public function getAccessList()
    {
        return array_merge($this->module->getAccessList(), $this->accessList);
    }
}