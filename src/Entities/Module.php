<?php

namespace Entities;

class Module
{
    /**
     * @var ModulePart[]
     */
    private $parts;

    /**
     * @var Permission[]
     */
    private $accessList;

    /**
     * @var string
     */
    private $name;

    /**
     * Module constructor.
     * @param $name
     * @param $accessList
     */
    public function __construct($name, $accessList)
    {
        $this->accessList = $accessList;
        $this->name = $name;
    }

    /**
     * @param $name
     * @param $accessList
     */
    public function addPart($name, $accessList)
    {
        $this->parts[] = new ModulePart($this, $name, $accessList);
    }

    /**
     * @return ModulePart[]
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * @param $name
     * @return ModulePart
     * @throws \Exception
     */
    public function getPartByName($name)
    {
        foreach ($this->parts as $part) {
            if ($part->getName() === $name) {
                return $part;
            }
        }

        throw new \Exception('Not found module part');
    }

    /**
     * @return Permission[]
     */
    public function getAccessList()
    {
        return $this->accessList;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}