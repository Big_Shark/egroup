<?php

namespace Entities;

class User
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var Role
     */
    private $role;

    /**
     * @var Permission[]
     */
    protected $permissions;

    /**
     * User constructor.
     * @param $username
     * @param Role $role
     * @param $permissions
     */
    public function __construct($username, Role $role, $permissions)
    {
        $this->username = $username;
        $this->role = $role;
        $this->permissions = $permissions;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    public function getPermissions()
    {
        return array_merge($this->getRole()->getPermissions(), $this->permissions);
    }
}