<?php


use Repositories\DBModuleRepository;
use Repositories\DBUserRepository;

class Service
{
    /**
     * @var Manager
     */
    private $manager;
    /**
     * @var DBUserRepository
     */
    private $userRepository;
    /**
     * @var DBModuleRepository
     */
    private $moduleRepository;

    public function __construct(Manager $manager, DBUserRepository $userRepository, DBModuleRepository $moduleRepository)
    {
        $this->manager = $manager;
        $this->userRepository = $userRepository;
        $this->moduleRepository = $moduleRepository;
    }

    public function can($username, $partName)
    {
        $user = $this->userRepository->getUser($username);
        $part = $this->moduleRepository->getPart($partName);
        return $this->manager->can($user, $part);
    }
}