<?php

namespace Repositories;

use Entities\Module;
use Entities\ModulePart;
use Entities\Permission;
use Entities\Role;
use Entities\User;
use Mappers\UserMapper;

class DBModuleRepository
{

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getModule($moduleName)
    {
        $query = 'SELECT 
            `modules`.`name` as `module_name`,
            `module_parts`.`name` as `module_part_name`,
            `module_permissions`.`name` as `module_permissions`,
            `module_part_permissions`.`name` as `module_part_permissions`
            FROM `modules`
            LEFT JOIN `module_parts` ON `module_parts`.`module_name` = `modules`.`name`
            LEFT JOIN `module_permission` ON `module_permission`.`module_name` = `modules`.`name`
            LEFT JOIN `permissions` as `module_permissions` ON `module_permissions`.`name` = `module_permission`.`permission_name`
            LEFT JOIN `module_part_permission` ON `module_part_permission`.`module_part_name` = `module_parts`.`name`
            LEFT JOIN `permissions` as `module_part_permissions` ON `module_part_permissions`.`name` = `module_part_permission`.`permission_name`
            WHERE `modules`.`name` = :moduleName
            ';

        //

        $sth = $this->pdo->prepare($query);
        $sth->bindParam(':moduleName', $moduleName, \PDO::PARAM_STR);
        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }


        // Todo: Refact this code, because thi is trash
        $data = [
            'module_permissions' => [],
            'parts' => []
        ];
        foreach ($sth->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $data['module'] =  $row['module_name'];
            if (!array_key_exists($row['module_part_name'], $data['parts'])) {
                $data['parts'][$row['module_part_name']] = [];
            }

            $row['module_permissions'] && $data['module_permissions'] += new Permission($row['module_permissions']);
            $row['module_part_permissions'] && $data['parts'][$row['module_part_name']][] = new Permission($row['module_part_permissions']);
        }

        $module = new Module($data['module'], $data['module_permissions']);
        foreach ($data['parts'] as $name => $permissions) {
            $module->addPart($name, $permissions);
        }

        return $module;
    }

    public function getPart($partName)
    {
        $query = 'SELECT 
            `module_parts`.`module_name` as `module_name`
            FROM `module_parts`
            WHERE `module_parts`.`name` = :partName
            LIMIT 1
            ';

        $sth = $this->pdo->prepare($query);
        $sth->bindParam(':partName', $partName, \PDO::PARAM_STR);
        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $this->getModule($result['module_name'])->getPartByName($partName);
    }
}