<?php

namespace Repositories;

use Entities\Permission;
use Entities\Role;
use Entities\User;
use Mappers\UserMapper;

class DBUserRepository
{

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * DBUserRepository constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $username
     * @return User
     * @throws \Exception
     */
    public function getUser($username)
    {
        $query = 'SELECT 
            `users`.`username` as `user_username`,
            `users`.`role` as `user_role`,
            `user_permissions`.`name` as `user_permissions`,
            `role_permissions`.`name` as `role_permissions`
            FROM `users`
            LEFT JOIN `permission_user` ON `permission_user`.`user_username` = `users`.`username`
            LEFT JOIN `permissions` as `user_permissions` ON `user_permissions`.`name` = `permission_user`.`permission_name`
            LEFT JOIN `role_permission` ON `role_permission`.`role_name` = `users`.`role`
            LEFT JOIN `permissions` as `role_permissions` ON `role_permissions`.`name` = `role_permission`.`permission_name`
            WHERE `users`.`username` = :username
            ';

        //

        $sth = $this->pdo->prepare($query);
        $sth->bindParam(':username', $username, \PDO::PARAM_STR);
        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }


        // Todo: Refact this code, because we create new class every each
        $userPermissions = [];
        $rolePermissions = [];
        foreach ($sth->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $row['user_permissions'] && $userPermissions[] = new Permission($row['user_permissions']);
            $row['role_permissions'] && $rolePermissions[] = new Permission($row['role_permissions']);
            $user = new User($row['user_username'], new Role($row['user_role'], array_unique($rolePermissions)), array_unique($userPermissions));
        }

        return $user;
    }
}