<?php

namespace Tests;

class ManagerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider canProvider
     */
    public function testCan($role, $permissions, $module, $accessList, $result)
    {
        $manage = new \Manager();

        $user = new \Entities\User('Test', $role, $permissions);
        $part = new \Entities\ModulePart($module, 'test part', $accessList);

        $this->assertEquals($result, $manage->can($user, $part));
    }

    public function canProvider()
    {
        return [
            [// Permission and part
                new \Entities\Role('admin', []),
                [new \Entities\Permission('test')],
                new \Entities\Module('Test Module', []),
                [new \Entities\Permission('test')],
                true
            ],
            [//Role and part
                new \Entities\Role('admin', [new \Entities\Permission('test')]),
                [],
                new \Entities\Module('Test Module', []),
                [new \Entities\Permission('test')],
                true
            ],
            [//Both and part
                new \Entities\Role('admin', [new \Entities\Permission('test')]),
                [new \Entities\Permission('test')],
                new \Entities\Module('Test Module', []),
                [new \Entities\Permission('test')],
                true
            ],

            [// Permission and module
                new \Entities\Role('admin', []),
                [new \Entities\Permission('test')],
                new \Entities\Module('Test Module', [new \Entities\Permission('test')]),
                [],
                true
            ],
            [//Role and module
                new \Entities\Role('admin', [new \Entities\Permission('test')]),
                [],
                new \Entities\Module('Test Module', [new \Entities\Permission('test')]),
                [],
                true
            ],
            [//Both and module
                new \Entities\Role('admin', [new \Entities\Permission('test')]),
                [new \Entities\Permission('test')],
                new \Entities\Module('Test Module', [new \Entities\Permission('test')]),
                [],
                true
            ],

            [//no permissions
                new \Entities\Role('admin', []),
                [],
                new \Entities\Module('Test Module', []),
                [new \Entities\Permission('test')],
                false
            ],
            [//wrong permissions
                new \Entities\Role('admin', []),
                [new \Entities\Permission('wrong_test')],
                new \Entities\Module('Test Module', []),
                [new \Entities\Permission('test')],
                false
            ],
        ];
    }
}