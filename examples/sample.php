<?php

include __DIR__.'/../vendor/autoload.php';

$pdo = new PDO('mysql:dbname=egroup_test;host=127.0.0.1', 'root', 'root');

$userRepository = new \Repositories\DBUserRepository($pdo);
$moduleRepository = new \Repositories\DBModuleRepository($pdo);

$service = new Service(new Manager(), $userRepository, $moduleRepository);
var_dump($service->can('test', 'read_article'));//true
var_dump($service->can('test', 'write_article'));//true
var_dump($service->can('test2', 'read_article'));//true
var_dump($service->can('test2', 'write_article'));//false